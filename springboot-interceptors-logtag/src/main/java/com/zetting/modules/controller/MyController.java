package com.zetting.modules.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 打出日志标识
 *
 * @author zetting
 * @date 2018-12-21 22:30
 */
@RestController
public class MyController {
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @GetMapping(value = "/logtag")
    public String logtag() {
        log.error("log 日志1");
        log.info("log 日志2 ");
        return "success";
    }
}
