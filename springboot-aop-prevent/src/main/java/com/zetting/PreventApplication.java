package com.zetting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动
 *
 * @author zetting
 * @date:2018/12/27
 */
@SpringBootApplication
public class PreventApplication {

    public static void main(String[] args) {

        SpringApplication.run(PreventApplication.class, args);

    }

}
