package com.zetting.modules.dto;


/**
 * 测试入参
 *
 * @author: zetting
 * @date:2018/12/27
 */
public class TestRequest {
    /**
     * 手机号码
     */
    private String mobile;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
