package com.zetting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *  启动类
 *   @author: zetting
 *   @date: 2018/12/20 22:41
 */
@SpringBootApplication
public class EnumValidateApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnumValidateApplication.class, args);
	}
}
